# @cloudweight/admin-panel

## Configuration

### dotenv

To configure local development environment create `.env` file with following contents:
```
SENTRY_ENV=local
DIETITIAN_API_URL=http://localhost:4000
COOKBOOK_API_URL=http://localhost:4010
MEAL_PLANNER_API_URL=http://localhost:4020/meal-planner
AUTH_DOMAIN=dietetykwchmurze-stage.eu.auth0.com
AUTH_CLIENT_ID=raJ3G3TGLbkvEvCzWXg9Zsph64YHCega
AUTH_AUDIENCE=api.stage.dietetykwchmurze.pl
```
