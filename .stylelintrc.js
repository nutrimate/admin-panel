module.exports = {
  extends: [
    require.resolve('stylelint-config-standard'),
  ],
  plugins: [
    require.resolve('stylelint-media-use-custom-media'),
  ],
  rules: {
    /* quotes configuration */
    'font-family-name-quotes': 'always-where-recommended',
    'function-url-quotes': 'always',
    'selector-attribute-quotes': 'always',
    'string-quotes': ['double', { "avoidEscape": true }],
    /* autoprefixer */
    'at-rule-no-vendor-prefix': true,
    'media-feature-name-no-vendor-prefix': true,
    'property-no-vendor-prefix': true,
    'selector-no-vendor-prefix': true,
    'value-no-vendor-prefix': true,
    /* specificity */
    'max-nesting-depth': 1,
    'selector-max-specificity': '1,3,1',
    'declaration-no-important': true,
    'declaration-property-unit-whitelist': [
      { "font-size": ["rem", "em"] },
      { "/^padding/": ["em"] },
      { "/^margin/": ["em"] },
      { "/^animation/": ["s"] },
      { "line-height": [] },
    ],
    'selector-max-attribute': 2,
    'selector-max-class': 2,
    'selector-max-combinators': 2,
    'selector-max-compound-selectors': 2,
    'selector-max-empty-lines': 0,
    'selector-max-id': 1,
    'selector-max-pseudo-class': 2,
    'selector-max-type': 1,
    'selector-max-universal': 1,
    'selector-no-qualifying-type': [
      true,
      {
        "ignore": "attribute"
      }
    ],
    'font-weight-notation': 'numeric',
    'max-line-length': 140,
    'csstools/media-use-custom-media': ['known', {
      importFrom: [require.resolve('./src/index.css')],
    }],
  }
}
