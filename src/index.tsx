import './sentry'
import 'react-hot-loader'
import React, { lazy, Suspense } from 'react'
import { render } from 'react-dom'
import { Auth0Provider, withAuthenticationRequired } from '@auth0/auth0-react'
import { config } from './config'
import './index.css'
import { requiredPermissions } from './pages'

const AppLazy = withAuthenticationRequired(
  lazy(() => import('./pages').then(m => ({ default: m.AppBootstrap })))
)

render(
  (
    <Auth0Provider
      domain={config.auth.domain || ''}
      clientId={config.auth.clientId || ''}
      redirectUri={window.location.origin}
      audience={config.auth.audience}
      scope={requiredPermissions.join(' ')}
    >
      <Suspense fallback={null}>
        <AppLazy />
      </Suspense>
    </Auth0Provider>
  ),
  document.getElementById('root')
)
