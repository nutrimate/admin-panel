import { parseISO } from 'date-fns'
import { config } from '~/config'
import { User, WeeklyDiet } from '~/models'
import { AccessTokenProvider, createHttpClient, HttpClient } from '../shared'

export class DietitianApiClient {
  private readonly httpClient: HttpClient

  constructor (accessTokenProvider: AccessTokenProvider) {
    this.httpClient = createHttpClient({
      baseURL: config.dietitianApiUrl,
      accessTokenProvider
    })
  }

  getDiets (): Promise<WeeklyDiet[]> {
    return this.httpClient.get('/diets')
      .then(r => r.data)
      .then(ds => ds.map((d: Omit<WeeklyDiet, 'weekStartDay'> & { weekStartDay: string }) => ({
        ...d,
        weekStartDay: parseISO(d.weekStartDay)
      })))
  }

  createDiet (diet: WeeklyDiet): Promise<void> {
    return this.httpClient.post('/diets', diet)
  }

  updateDiet (diet: WeeklyDiet): Promise<void> {
    return this.httpClient.put('/diets', diet)
  }

  getUsers (): Promise<User[]> {
    return this.httpClient.get('/users')
      .then(r => r.data)
  }
}
