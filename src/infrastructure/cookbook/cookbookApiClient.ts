import { config } from '~/config'
import { Ingredient, Unit, Recipe, MealType, NormalizedRecipe } from '~/models'
import { AccessTokenProvider, HttpClient, createHttpClient } from '../shared'

export class CookbookApiClient {
  private readonly httpClient: HttpClient

  constructor (accessTokenProvider: AccessTokenProvider) {
    this.httpClient = createHttpClient({
      baseURL: config.cookbookApiUrl,
      accessTokenProvider
    })
  }

  getAllUnits (): Promise<Unit[]> {
    return this.httpClient
      .get<Unit[]>('/units')
      .then(r => r.data)
  }

  createUnit (unit: Unit): Promise<void> {
    return this.httpClient
      .post('/units', unit)
  }

  updateUnit ({ id, ...updated }: Unit): Promise<void> {
    return this.httpClient
      .put(`/units/${id}`, updated)
  }

  getAllIngredients (): Promise<Ingredient[]> {
    return this.httpClient
      .get<Ingredient[]>('/ingredients')
      .then(r => r.data)
  }

  createIngredient (ingredient: Ingredient): Promise<void> {
    return this.httpClient
      .post('/ingredients', ingredient)
  }

  updateIngredient ({ id, ...updated }: Ingredient): Promise<void> {
    return this.httpClient
      .put(`/ingredients/${id}`, updated)
  }

  getAllRecipes (): Promise<Recipe[]> {
    return this.httpClient
      .get<Recipe[]>('/recipes')
      .then(r => r.data)
  }

  createRecipe (recipe: NormalizedRecipe): Promise<void> {
    return this.httpClient
      .post('/recipes', recipe)
  }

  updateRecipe ({ id, ...updated }: NormalizedRecipe): Promise<void> {
    return this.httpClient
      .put(`/recipes/${id}`, updated)
  }

  getMealTypes (): Promise<MealType[]> {
    return this.httpClient
      .get<MealType[]>('/meal-types')
      .then(r => r.data)
  }
}
