import React, { FC, createContext, useContext } from 'react'
import { CookbookApiClient } from './cookbookApiClient'

const CookbookApiClientContext = createContext<CookbookApiClient | null>(null)

export const ProvideCookbookApiClient: FC<{ cookbookApiClient: CookbookApiClient }> = props => {
  const { cookbookApiClient, children } = props

  return (
    <CookbookApiClientContext.Provider value={cookbookApiClient}>
      {children}
    </CookbookApiClientContext.Provider>
  )
}

export const useCookbookApiClient = (): CookbookApiClient => {
  const cookbookApiClient = useContext(CookbookApiClientContext)

  if (cookbookApiClient == null) {
    throw new Error('Tried to use useCookbookApiClient hook without initializing CookbookApiClient')
  }

  return cookbookApiClient
}
