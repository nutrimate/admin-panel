import axios, { AxiosInstance } from 'axios'

export type HttpClient = AxiosInstance
export type AccessTokenProvider = () => Promise<string>

type Options = {
  baseURL?: string;
  accessTokenProvider: AccessTokenProvider;
}

export const createHttpClient = (options: Options): HttpClient => {
  const { baseURL, accessTokenProvider } = options
  const httpClient = axios.create({ baseURL })

  httpClient.interceptors.request
    .use(async config => {
      const accessToken = await accessTokenProvider()

      config.headers.Authorization = `Bearer ${accessToken}`
      return config
    })

  return httpClient
}
