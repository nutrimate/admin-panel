import { AxiosError } from 'axios'
import { config } from '~/config'
import { GeneratedDiet, PlanDietRequest } from '~/models'
import { AccessTokenProvider, createHttpClient, HttpClient } from '../shared'

type GenerateDietResult =
  {
    result: GeneratedDiet;
    status: 'success';
  } |
  {
    result: string;
    status: 'failure';
  }

export class MealPlannerApiClient {
  private readonly httpClient: HttpClient

  constructor (accessTokenProvider: AccessTokenProvider) {
    this.httpClient = createHttpClient({
      baseURL: config.mealPlannerApiUrl,
      accessTokenProvider
    })
  }

  planDiet (request: PlanDietRequest): Promise<GeneratedDiet> {
    return this.httpClient
      .post<GenerateDietResult>('/plan-diet', request)
      .then(r => {
        const data = r.data
        if (data.status !== 'success') {
          throw new Error(data.result)
        }

        return data.result
      })
      .catch((error: AxiosError | Error) => {
        const data = 'response' in error && error.response && error.response.data

        if (data && data.status) {
          throw new Error(data.result)
        }

        throw error
      })
  }
}
