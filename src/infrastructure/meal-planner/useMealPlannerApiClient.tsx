import React, { FC, createContext, useContext } from 'react'
import { MealPlannerApiClient } from './mealPlannerApiClient'

const MealPlannerApiClientContext = createContext<MealPlannerApiClient | null>(null)

export const ProvideMealPlannerApiClient: FC<{ mealPlannerApiClient: MealPlannerApiClient }> = props => {
  const { mealPlannerApiClient, children } = props

  return (
    <MealPlannerApiClientContext.Provider value={mealPlannerApiClient}>
      {children}
    </MealPlannerApiClientContext.Provider>
  )
}

export const useMealPlannerApiClient = (): MealPlannerApiClient => {
  const mealPlannerApiClient = useContext(MealPlannerApiClientContext)

  if (mealPlannerApiClient == null) {
    throw new Error('Tried to use useMealPlannerApiClient hook without initializing MealPlannerApiClient')
  }

  return mealPlannerApiClient
}
