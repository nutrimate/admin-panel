export type MealType = {
  id: string;
  name: string;
}
