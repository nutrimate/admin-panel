import { MealType } from '../cookbook'
import { User } from './user'
import { MealPlan } from './mealPlan'

export type WeeklyDiet = {
  id: string;
  userId: string;
  weekStartDay: Date;
  mealSchedule: MealType[];
  targetCalories: number;
  mealPlan: MealPlan;
}

export type WeeklyUserDiet = WeeklyDiet & {
  user: User;
}
