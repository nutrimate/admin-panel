import { getRecipeNutritionFacts, MealType, NutritionFacts, Recipe } from '../cookbook'

export type MealPlanEntry = {
  day: number;
  mealNumber: number;
  meal: MealType;
  recipe: Recipe;
};

export type MealPlan = MealPlanEntry[];

export type DailyMealPlan = { [mealNumber: number]: MealPlanEntry }
export type OrderedMealPlan = { [day: number]: DailyMealPlan }

export const orderMealPlan = (mealPlan: MealPlan): OrderedMealPlan =>
  mealPlan.reduce(
    (orderedMealPlan, entry) => {
      const { day, mealNumber } = entry

      const dayEntry = {
        ...orderedMealPlan[day],
        [mealNumber]: entry
      }

      return { ...orderedMealPlan, [day]: dayEntry }
    },
    {} as OrderedMealPlan
  )

export const getMealPlanTotalNutritions = (mealPlan: MealPlanEntry[]): NutritionFacts =>
  mealPlan.map(p => p.recipe)
    .reduce(
      (total, recipe) => {
        const { calories, carbohydrates, fats, proteins } = getRecipeNutritionFacts(recipe)

        return {
          calories: total.calories + calories,
          carbohydrates: total.carbohydrates + carbohydrates,
          fats: total.fats + fats,
          proteins: total.proteins + proteins
        }
      },
      {
        calories: 0,
        proteins: 0,
        fats: 0,
        carbohydrates: 0
      }
    )
