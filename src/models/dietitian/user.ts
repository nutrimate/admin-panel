export type User = {
  id: string;
  name: string;
  email: string;
  pictureUrl: string;
}
