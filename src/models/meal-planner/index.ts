export type GeneratedDiet = { day: number; recipeId: string; mealTypeId: string }[]

export enum DietGoal {
  Loss = 'lose-weight',
  Sustain = 'sustain',
  Gain = 'gain-weight'
}

export type PlanDietRequest = {
  mealSchedule: string[];
  dailyCalorieIntake: number;
  dailyMinProteins: number;
  dailyMaxProteins: number;
  dailyMinFats: number;
  days: number;
  goal: DietGoal;
  excludedIngredients: string;
}
