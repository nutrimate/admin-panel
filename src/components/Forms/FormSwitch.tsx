import React, { FC } from 'react'
import { useField } from 'formik'
import { FormControl, FormControlLabel, FormHelperText, Switch } from '@material-ui/core'

export type FormSwitchProps = {
  name: string;
  label: string;
  helperText?: string;
}

export const FormSwitch: FC<FormSwitchProps> = (props) => {
  const [field, meta, helpers] = useField(props)
  const hasError = !!(meta.touched && meta.error)
  const helperText = hasError ? meta.error : props.helperText
  return (
    <FormControl error={hasError}>
      <FormControlLabel
        control={
          <Switch
            {...field}
            checked={field.value}
            onChange={(_, v) => helpers.setValue(v)}
            color='primary'
          />
        }
        label={props.label}
      />
      {helperText && (<FormHelperText>{helperText}</FormHelperText>)}
    </FormControl>
  )
}
