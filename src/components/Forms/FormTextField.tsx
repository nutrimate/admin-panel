import React, { FC } from 'react'
import { TextField } from '@material-ui/core'
import { useField } from 'formik'

export type FormTextFieldProps = {
  name: string;
  type?: string;
  label: string;
  placeholder: string;
  helperText?: string;
  className?: string;
  fullWidth?: boolean;
}

export const FormTextField: FC<FormTextFieldProps> = (props) => {
  const [field, meta] = useField(props)
  const hasError = !!(meta.touched && meta.error)
  return (
    <TextField
      variant='outlined'
      margin='normal'
      fullWidth
      {...field}
      {...props}
      error={hasError}
      helperText={hasError ? meta.error : props.helperText}
    />
  )
}
