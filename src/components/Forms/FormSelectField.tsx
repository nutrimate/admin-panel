import React, { FC } from 'react'
import { MenuItem, Select, FormControl, InputLabel, FormHelperText, SelectProps } from '@material-ui/core'
import { useField } from 'formik'

export type FormSelectFieldProps = {
  name: string;
  label: string;
  options: { key: string; value: string }[];
  helperText?: string;
  className?: string;
  selectProps?: SelectProps;
  fullWidth?: boolean;
}

export const FormSelectField: FC<FormSelectFieldProps> = (props) => {
  const [field, meta] = useField(props)
  const hasError = !!(meta.touched && meta.error)
  const helperText = hasError ? meta.error : props.helperText

  return (
    <FormControl variant='outlined' error={hasError} className={props.className} margin='normal' fullWidth={props.fullWidth == null ? true : props.fullWidth}>
      <InputLabel id={`select-${props.name}-label`}>{props.label}</InputLabel>
      <Select
        {...props.selectProps}
        labelId={`select-${props.name}-label`}
        id={`select-${props.name}`}
        {...field}
        label={props.label}
      >
        {
          props.options.map(({ key, value }) => (
            <MenuItem key={key} value={value}>
              {value === '' ? (<span style={{ opacity: 0.42 }}>{key}</span>) : key}
            </MenuItem>
          ))
        }
      </Select>
      {helperText && (<FormHelperText>{helperText}</FormHelperText>)}
    </FormControl>
  )
}
