import React, { ReactElement } from 'react'
import { TextField } from '@material-ui/core'
import { Autocomplete } from '@material-ui/lab'
import { useField } from 'formik'

export type FormAutocompleteProps<T> = {
  name: string;
  label: string;
  placeholder: string;
  options: T[];
  className?: string;
  helperText?: string;
  getOptionLabel: (option: T) => string;
  getOptionValue: (option: T) => string;
  onChange?: (value: null | string | string[]) => void;
}

export const FormAutocomplete = function<T> (props: FormAutocompleteProps<T>): ReactElement {
  const { label, placeholder, options, getOptionLabel, getOptionValue, className, onChange, helperText, ...rest } = props
  const [field, meta, helpers] = useField(rest)
  const hasError = !!(meta.touched && meta.error)

  return (
    <Autocomplete
      {...field}
      value={options.find(o => getOptionValue(o) === field.value)}
      onChange={(_, option) => {
        if (option == null) {
          helpers.setValue(null)
          return onChange && onChange(null)
        }

        const value = Array.isArray(option) ? option.map(getOptionValue) : getOptionValue(option)
        helpers.setValue(value)
        onChange && onChange(value)
      }}
      options={options}
      getOptionLabel={getOptionLabel}
      renderInput={(params) => (
        <TextField
          {...params}
          label={label}
          placeholder={placeholder}
          variant='outlined'
          margin='normal'
          fullWidth
          error={hasError}
          helperText={hasError ? meta.error : helperText}
        />
      )}
      className={className}
    />
  )
}
