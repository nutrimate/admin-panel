import React, { ElementType, FC, forwardRef, ForwardRefRenderFunction } from 'react'
import { ListItem, ListItemProps } from '@material-ui/core'
import { Link } from 'react-router-dom'

export const ListItemLink: FC<ListItemProps<Link, { button: true }>> = ({ to, ...props }) => {
  const renderLink: ElementType = React.useMemo(
    () => {
      const link: ForwardRefRenderFunction<HTMLAnchorElement> = (itemProps, ref) => (
        <Link to={to} ref={ref} {...itemProps} />
      )

      return forwardRef(link)
    },
    [to]
  )

  return (
    <ListItem component={renderLink} {...props} />
  )
}
