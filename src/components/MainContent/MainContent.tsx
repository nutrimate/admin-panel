import React, { FC, ReactNode } from 'react'
import { CircularProgress, Paper } from '@material-ui/core'
import classNames from 'classnames'
import './main-content.css'

export type MainContentProps = {
  error?: Error;
  loading: boolean;
  children: ReactNode;
  padded?: boolean;
}

export const MainContent: FC<MainContentProps> = props => {
  const { error, loading, padded, children } = props

  if (error) {
    return (
      <Paper className='main-content main-content--error'>
        {error.message}
      </Paper>
    )
  }

  if (loading) {
    return (
      <Paper className='main-content'>
        <div className='main-content__loader'>
          <CircularProgress />
        </div>
      </Paper>
    )
  }

  const classes = classNames('main-content', { 'main-content--padded': padded })

  return (
    <Paper className={classes}>
      {children}
    </Paper>
  )
}
