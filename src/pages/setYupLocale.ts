/* eslint-disable no-template-curly-in-string */
import { setLocale, LocaleObject } from 'yup'

const locale: LocaleObject = {
  mixed: {
    default: 'Pole ${path} jest nieprawidłowe',
    required: 'Pole ${path} jest wymagane',
    // eslint-disable-next-line no-irregular-whitespace
    notType: ({ path, type }) => type === 'number' ? `Pole ${path} musi być liczbą` : `Pole ${path} ma nieprawidłową wartość`
  },
  number: {
    min: 'Wartość pola ${path} musi być większa niż ${min}',
    integer: 'Wartość pola ${path} musi być liczbą całkowitą',
    max: 'Wartość pola ${path} musi być mniejsza niż ${max}',
    moreThan: 'Wartość pola ${path} musi być większa niż ${more}'
  },
  string: {
    email: '${path} musi być prawidłowym adresem e-mail'
  }
}

setLocale(locale)
