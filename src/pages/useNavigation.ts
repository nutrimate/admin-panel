import { matchPath, useLocation } from 'react-router-dom'
import { routes as cookbookRoutes } from './Cookbook'
import { routes as dietitianRoutes } from './Dietitian'

const routeGroups = [
  {
    title: 'Książka kucharska',
    items: [
      { name: 'Przepisy', url: cookbookRoutes.recipes.home() },
      { name: 'Składniki', url: cookbookRoutes.ingredients.home() },
      { name: 'Jednostki', url: cookbookRoutes.units.home() }
    ]
  },
  {
    title: 'Dietetyk',
    items: [
      { name: 'Diety', url: dietitianRoutes.home() }
    ]
  }
]

export type NavigationSectionItem ={ name: string; url: string; isActive: boolean }

export type NavigationSection = {
  title: string;
  items: NavigationSectionItem[];
}

export type UseNavigationResult = {
  sections: NavigationSection[];
  activeItem: NavigationSectionItem | undefined;
}

export const useNavigation = (): UseNavigationResult => {
  const location = useLocation()
  const sections = routeGroups
    .map(section => ({
      title: section.title,
      items: section.items
        .map(item => {
          const match = matchPath(location.pathname, { path: item.url })
          return {
            name: item.name,
            url: item.url,
            isActive: match != null
          }
        })
    }))

  const activeItem = sections
    .reduce((is, s) => is.concat(s.items), [] as NavigationSectionItem[])
    .find(i => i.isActive)

  return {
    sections,
    activeItem
  }
}
