import React, { FC, useState } from 'react'
import { AppBar, CssBaseline, Drawer, Hidden, IconButton, Toolbar, Typography } from '@material-ui/core'
import { Menu } from '@material-ui/icons'
import { Navigation, NavigationSection } from './Navigation'
import './app-shell.css'

export type AppShellProps = {
  title: string;
  navigationSections: NavigationSection[];
}

export const AppShell: FC<AppShellProps> = ({ children, title, navigationSections }) => {
  const [mobileOpen, setMobileOpen] = useState(false)

  const handleDrawerToggle = (): void => {
    setMobileOpen(!mobileOpen)
  }

  const drawerContents = <Navigation sections={navigationSections} />

  return (
    <div className='app-shell'>
      <CssBaseline />
      <AppBar position='fixed' className='app-shell__app-bar'>
        <Toolbar>
          <IconButton
            color='inherit'
            aria-label='open drawer'
            edge='start'
            onClick={handleDrawerToggle}
            className='navigation-menu-icon'
          >
            <Menu />
          </IconButton>
          <Typography variant='h6' noWrap>
            {title}
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className='app-shell__navigation' aria-label='navigation'>
        <Hidden mdUp implementation='css'>
          <Drawer
            variant='temporary'
            anchor='left'
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{ keepMounted: true }}
          >
            {drawerContents}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation='css'>
          <Drawer
            variant='permanent'
            open
            classes={{ paper: 'navigation__paper' }}
          >
            {drawerContents}
          </Drawer>
        </Hidden>
      </nav>
      <main className='app-shell__content'>
        {children}
      </main>
    </div>
  )
}
