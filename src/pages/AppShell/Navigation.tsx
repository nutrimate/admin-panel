import React, { FC, Fragment } from 'react'
import { Divider, List, ListItemText, ListSubheader } from '@material-ui/core'
import { ListItemLink } from '~/components'
import logo from '~/assets/logo.svg'
import './navigation.css'

export type NavigationSection = {
  title: string;
  items: { name: string; url: string; isActive: boolean }[];
}

export type NavigationProps = {
  sections: NavigationSection[];
}

export const Navigation: FC<NavigationProps> = ({ sections }) => {
  return (
    <div>
      <img className='app-shell-navigation__logo' src={logo} alt='logo' />
      {
        sections.map(({ title, items }) => (
          <Fragment key={title}>
            <Divider />
            <List
              subheader={
                <ListSubheader>
                  {title}
                </ListSubheader>
              }
            >
              {
                items.map(({ name, url, isActive }) => (
                  <ListItemLink key={url} to={url} button selected={isActive}>
                    <ListItemText primary={name} />
                  </ListItemLink>
                ))
              }
            </List>
          </Fragment>
        ))
      }
    </div>
  )
}
