import React, { FC } from 'react'
import { Typography } from '@material-ui/core'
import './no-access.css'

const noAccessText = 'Nie masz dostępu do tej aplikacji' // TODO: i18n

export const NoAccess: FC = () => {
  return (
    <Typography className='no-access'>
      {noAccessText}
    </Typography>
  )
}
