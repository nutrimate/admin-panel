import './setYupLocale'
import { hot } from 'react-hot-loader'
import React, { FC } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { StylesProvider } from '@material-ui/core'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import { useAuth0 } from '@auth0/auth0-react'
import DateFnsUtils from '@date-io/date-fns'
// eslint-disable-next-line import/no-internal-modules
import plLocale from 'date-fns/locale/pl'
import {
  CookbookApiClient, DietitianApiClient,
  MealPlannerApiClient,
  ProvideCookbookApiClient, ProvideDietitianApiClient,
  ProvideMealPlannerApiClient
} from '../infrastructure'
import { App } from './App'
import { useThemeCssVariables } from './cssVariables'

const AppBootstrap: FC = () => {
  const styles = useThemeCssVariables()
  const { getAccessTokenSilently } = useAuth0()

  const accessTokenProvider = (): Promise<string> => getAccessTokenSilently()
  const cookbookApiClient = new CookbookApiClient(accessTokenProvider)
  const mealPlannerApiClient = new MealPlannerApiClient(accessTokenProvider)
  const dietitianApiClient = new DietitianApiClient(accessTokenProvider)

  return (
    <ProvideCookbookApiClient cookbookApiClient={cookbookApiClient}>
      <ProvideMealPlannerApiClient mealPlannerApiClient={mealPlannerApiClient}>
        <ProvideDietitianApiClient dietitianApiClient={dietitianApiClient}>
          <StylesProvider injectFirst>
            <MuiPickersUtilsProvider utils={DateFnsUtils} locale={plLocale}>
              <Router>
                <div className={styles.root}>
                  <App />
                </div>
              </Router>
            </MuiPickersUtilsProvider>
          </StylesProvider>
        </ProvideDietitianApiClient>
      </ProvideMealPlannerApiClient>
    </ProvideCookbookApiClient>
  )
}

const HotModuleAppBootstrap = hot(module)(AppBootstrap)

export { HotModuleAppBootstrap as AppBootstrap }
