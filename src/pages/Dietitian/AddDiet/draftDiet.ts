import { MealPlan, MealType } from '~/models'

export type DraftDiet = {
    mealSchedule: MealType[];
    targetCalories: number;
    mealPlan: MealPlan;
}
