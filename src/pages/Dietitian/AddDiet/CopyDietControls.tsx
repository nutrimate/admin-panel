import React, { FC, useState } from 'react'
import { FormControl, InputLabel, Select } from '@material-ui/core'
import { useDiets } from '../shared'
import { DraftDiet } from './draftDiet'

export type CopyDietControlsProps = {
  userId: string;
  onDraftDietSelected: (diet: DraftDiet | undefined) => void;
}

export const CopyDietControls: FC<CopyDietControlsProps> = ({ userId, onDraftDietSelected }) => {
  const { diets = [] } = useDiets()
  const userDiets = diets.filter(d => d.user.id === userId)
  const [dietId, setDietId] = useState('')

  const handleDietIdChange = (dietId: string): void => {
    setDietId(dietId)

    const newDiet = userDiets.find(d => d.id === dietId)
    onDraftDietSelected(newDiet)
  }

  return (
    <FormControl variant='outlined' style={{ marginTop: '1em' }} fullWidth>
      <InputLabel htmlFor='source-diet'>Tydzień</InputLabel>
      <Select
        native
        value={dietId}
        onChange={event => handleDietIdChange(event.target.value as string)}
        name='sourceDiet'
        inputProps={{ id: 'source-diet' }}
      >
        <option aria-label='None' value=''>
          {userDiets.length === 0 ? 'Brak diet' : ''}
        </option>
        {
          userDiets.map(d => {
            const value = d.id

            return (
              <option key={value} value={value}>{d.weekStartDay.toLocaleDateString()}</option>
            )
          })
        }
      </Select>
    </FormControl>
  )
}
