import React, { FC } from 'react'
import { Typography } from '@material-ui/core'
import { Formik, FormikHelpers } from 'formik'
import { v4 as uuidV4 } from 'uuid'
import { startOfWeek } from 'date-fns'
import { MainContent } from '~/components'
import { WeeklyDiet } from '~/models'
import { useDiets } from '../shared'
import { AddDietForm, AddDietFormValues, DietSource, formSchema } from './AddDietForm'

export type AddDietProps = {
  onDietCreated: () => void;
}

export const AddDiet: FC<AddDietProps> = ({ onDietCreated }) => {
  const { diets, error, create } = useDiets()
  const initialValues: AddDietFormValues = {
    userId: '',
    weekStartDay: startOfWeek(new Date(), { weekStartsOn: 1 }),
    dietSource: DietSource.Copy,
    draftDiet: undefined
  }

  const handleSubmit = async (values: AddDietFormValues, helpers: FormikHelpers<AddDietFormValues>): Promise<void> => {
    helpers.setStatus(null)

    const diet = values.draftDiet
    if (!diet) {
      const error = 'Cannot save empty diet'
      helpers.setStatus(error)
      throw new Error(error)
    }

    const weeklyDiet: WeeklyDiet = {
      id: uuidV4(),
      weekStartDay: values.weekStartDay,
      userId: values.userId,
      targetCalories: diet.targetCalories,
      mealSchedule: diet.mealSchedule,
      mealPlan: diet.mealPlan
    }
    try {
      await create(weeklyDiet)
      onDietCreated()
    } catch (error) {
      helpers.setStatus(error.message)
      throw error
    }
  }

  return (
    <MainContent loading={diets === undefined} error={error} padded>
      <Typography>
        Skomponuj dietę
      </Typography>
      <Formik
        initialValues={initialValues}
        validationSchema={formSchema}
        onSubmit={handleSubmit}
      >
        {
          formik => (
            <AddDietForm {...formik} />
          )
        }
      </Formik>
    </MainContent>
  )
}
