import React, { FC } from 'react'
import { Formik, FormikHelpers } from 'formik'
import { Typography } from '@material-ui/core'
import { DietGoal } from '~/models'
import { DraftDiet } from '../draftDiet'
import { formSchema, GenerateDietForm, GenerateDietFormValues } from './GenerateDietForm'
import { useGeneratedDiet } from './useGeneratedDiet'

type GenerateDietControlsProps = {
  onDraftDietSelected: (diet: DraftDiet | undefined) => void;
}

export const GenerateDietControls: FC<GenerateDietControlsProps> = props => {
  const { onDraftDietSelected } = props
  const { error, generateDiet } = useGeneratedDiet()

  if (error) {
    return (
      <Typography color='error'>
        {error.message}
      </Typography>
    )
  }

  const initialValues: GenerateDietFormValues = {
    goal: DietGoal.Loss,
    minProteins: 0,
    maxProteins: 0,
    minFats: 0,
    calories: 0,
    excludedIngredients: []
  }

  const handleSubmit = async (values: GenerateDietFormValues, helpers: FormikHelpers<GenerateDietFormValues>): Promise<void> => {
    helpers.setStatus({ type: 'loading' })
    try {
      const draftDiet = await generateDiet(values)

      helpers.setStatus()
      onDraftDietSelected(draftDiet)
    } catch (error) {
      helpers.setStatus({ type: 'error', payload: error })
      onDraftDietSelected(undefined)
      throw error
    }
  }

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={formSchema}
      onSubmit={handleSubmit}
    >
      {
        formik => (
          <GenerateDietForm {...formik} />
        )
      }
    </Formik>
  )
}
