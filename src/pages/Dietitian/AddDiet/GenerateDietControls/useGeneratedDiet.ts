import { useMealPlannerApiClient } from '~/infrastructure'
import { MealPlanEntry, MealType } from '~/models'
import { useMealTypes, useRecipes } from '../../../shared'
import { DraftDiet } from '../draftDiet'
import { GenerateDietFormValues } from './GenerateDietForm'

export type GeneratedDietController = {
  error?: Error;
  generateDiet: (values: GenerateDietFormValues) => Promise<DraftDiet>;
}

export const useGeneratedDiet = (): GeneratedDietController => {
  const mealNames = [
    'Śniadanie',
    'Obiad',
    'Kolacja'
  ]

  const { mealTypes = [], error: mealTypesError } = useMealTypes()
  const { recipes = [], error: recipesError } = useRecipes()
  const mealPlannerApiClient = useMealPlannerApiClient()

  const generateDiet = async (values: GenerateDietFormValues): Promise<DraftDiet> => {
    const mealSchedule = mealNames.map(name => mealTypes.find(mt => mt.name === name))
      .filter((mt): mt is MealType => mt != null)

    if (mealSchedule.length !== mealNames.length) {
      throw new Error(`Invalid meal schedule: ${JSON.stringify(mealSchedule, null, 2)}`)
    }

    const generatedDiet = await mealPlannerApiClient.planDiet({
      goal: values.goal,
      dailyMinFats: values.minFats,
      dailyMinProteins: values.minProteins,
      dailyMaxProteins: values.maxProteins,
      dailyCalorieIntake: values.calories,
      days: 7,
      mealSchedule: mealSchedule.map(mt => mt.id),
      excludedIngredients: values.excludedIngredients.join(',')
    })

    const mealPlan = generatedDiet.map(d => {
      const mealType = mealTypes.find(mt => mt.id === d.mealTypeId)
      if (!mealType) {
        throw new Error(`Missing meal type with id = ${d.mealTypeId}`)
      }

      const recipe = recipes.find(r => r.id === d.recipeId)
      if (!recipe) {
        throw new Error(`Missing recipe with id = ${d.recipeId}`)
      }

      const mealPlanEntry: MealPlanEntry = {
        day: d.day,
        mealNumber: mealSchedule.findIndex(mt => mt.id === d.mealTypeId) + 1,
        meal: mealType,
        recipe
      }

      return mealPlanEntry
    })

    return {
      mealSchedule: mealSchedule,
      targetCalories: values.calories,
      mealPlan
    }
  }

  return {
    error: mealTypesError || recipesError,
    generateDiet
  }
}
