import { FormikProps } from 'formik'
import React, { FC } from 'react'
import { Box, Button, CircularProgress, Typography } from '@material-ui/core'
import * as yup from 'yup'
import { FormAutocomplete, FormSelectField, FormTextField } from '~/components'
import { DietGoal } from '~/models'
import { useIngredients } from '~/pages/Cookbook/shared'

export type GenerateDietFormValues = {
  calories: number;
  minProteins: number;
  maxProteins: number;
  minFats: number;
  goal: DietGoal;
  excludedIngredients: string[];
}

export type GenerateDietStatus = { type: 'loading' } | { type: 'error'; payload: Error }

const labels = {
  calories: 'Dzienne zapotrzebowanie kaloryczne',
  goal: 'Cel',
  minProteins: 'Dzienne zapotrzebowanie na białko (min)',
  maxProteins: 'Dzienne zapotrzebowanie na białko (max)',
  minFats: 'Dzienne zapotrzebowanie na tłuszcze (min)',
  excludedIngredients: 'Wykluczone składniki'
}

export const formSchema = yup.object<GenerateDietFormValues>().shape({
  calories: yup.number().required().moreThan(0).label(labels.calories),
  minProteins: yup.number().required().moreThan(0).label(labels.minProteins),
  maxProteins: yup.number().required().moreThan(yup.ref('minProteins')).label(labels.maxProteins),
  minFats: yup.number().required().moreThan(0).label(labels.minFats),
  goal: yup.mixed<DietGoal>().oneOf(Object.values(DietGoal)).required().label(labels.goal),
  excludedIngredients: yup.array().label(labels.excludedIngredients)
})

type GenerateDietFormProps = FormikProps<GenerateDietFormValues>

export const GenerateDietForm: FC<GenerateDietFormProps> = props => {
  const { handleSubmit, setFieldValue, values } = props
  const status = props.status as GenerateDietStatus | undefined
  const { ingredients } = useIngredients()

  const goalOptions = [
    { key: 'Schudnąć', value: DietGoal.Loss },
    { key: 'Utrzymać wagę', value: DietGoal.Sustain },
    { key: 'Przytyć', value: DietGoal.Gain }
  ]

  return (
    <div>
      <FormSelectField name='goal' label={labels.goal} options={goalOptions} />
      <FormTextField name='calories' type='number' label={labels.calories} placeholder='Wprowadź ilość kalorii' helperText='kcal' />
      <FormTextField name='minProteins' type='number' label={labels.minProteins} placeholder='Wprowadź minimalną ilość białka' helperText='g' />
      <FormTextField name='maxProteins' type='number' label={labels.maxProteins} placeholder='Wprowadź maksymalną ilość białka' helperText='g' />
      <FormTextField name='minFats' type='number' label={labels.minFats} placeholder='Wprowadź minimalną ilość tłuszczy' helperText='g' />
      {ingredients && (
        <FormAutocomplete
          name='excludedIngredients'
          label='Wykluczone składniki'
          placeholder='Wykluczone składniki'
          options={ingredients}
          getOptionLabel={ingredient => ingredient.name}
          getOptionValue={ingredient => ingredient.id}
          onChange={(value) => setFieldValue('excludedIngredients', [...values.excludedIngredients, value].filter(Boolean))}
          className='ingredients-list-item__ingredient'
        />
      )}
      <ul>
        {values.excludedIngredients.map((ingredient) =>
          <li key={ingredient}>
            {ingredients?.find(i => i.id === ingredient)?.name}
          </li>
        )}
      </ul>
      <Box display='flex' justifyContent='flex-end'>
        <Button onClick={() => handleSubmit()}>
          Wygeneruj dietę
        </Button>
      </Box>
      {
        status?.type === 'loading' && (
          <Box display='flex' justifyContent='center'>
            <CircularProgress />
          </Box>
        )
      }
      {
        status?.type === 'error' && (
          <Typography color='error'>
            {status.payload.message}
          </Typography>
        )
      }
    </div>
  )
}
