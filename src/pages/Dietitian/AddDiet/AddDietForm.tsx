import React, { FC } from 'react'
import {
  Box,
  Button,
  Divider,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Typography
} from '@material-ui/core'
import { FormikProps, useField } from 'formik'
import * as yup from 'yup'
import { DatePicker } from '@material-ui/pickers'
import { isSameDay, startOfWeek } from 'date-fns'
import { FormAutocomplete } from '~/components'
import { orderMealPlan, User } from '~/models'
import { useUsers, MealPlanPresenter } from '../shared'
import { CopyDietControls } from './CopyDietControls'
import { GenerateDietControls } from './GenerateDietControls'
import { DraftDiet } from './draftDiet'

export enum DietSource {
  Copy = 'copy',
  Generated = 'generated'
}

export type AddDietFormValues = {
  userId: string;
  weekStartDay: Date;
  dietSource: DietSource;
  draftDiet?: DraftDiet;
}

const labels = {
  userId: 'Użytkownik',
  dietSource: 'Źródło diety',
  week: 'Tydzień'
}

export const formSchema = yup.object<AddDietFormValues>().shape({
  userId: yup.string().required().label(labels.userId),
  weekStartDay: yup.date().required().label(labels.week),
  dietSource: yup.string().required().label(labels.dietSource),
  draftDiet: yup.object().required().label('Dieta')
})

const isInvalidDietStartDay = (day: Date | null): boolean => {
  if (!day) {
    return false
  }

  const weekStartDay = startOfWeek(day, { weekStartsOn: 1 })

  return !isSameDay(day, weekStartDay)
}

type AddDietFormProps = FormikProps<AddDietFormValues>

export const AddDietForm: FC<AddDietFormProps> = ({ values, setValues, handleSubmit, isSubmitting, status }) => {
  const { users = [] } = useUsers()
  const [,, dietField] = useField('draftDiet')
  const [weekStartDayFieldProps,, weekStartDayField] = useField<Date>('weekStartDay')
  const { draftDiet, dietSource } = values
  const orderedMealPlan = draftDiet && orderMealPlan(draftDiet.mealPlan)

  const handleDietSourceChange = (dietSource: DietSource): void => {
    setValues({
      ...values,
      draftDiet: undefined,
      dietSource
    })
  }
  const handleDietChange = (diet: DraftDiet | undefined): void => {
    dietField.setTouched(true)
    dietField.setValue(diet)
  }

  const handleWeekStartDayChange = (day: Date | null): void => {
    if (!day) {
      return
    }

    weekStartDayField.setValue(day)
  }

  return (
    <form onSubmit={handleSubmit}>
      <FormAutocomplete
        name='userId'
        label={labels.userId}
        placeholder='Wybierz użytkownika'
        options={users}
        getOptionLabel={(u: User) => `${u.name} (${u.email})`}
        getOptionValue={(u: User) => u.id}
      />
      <FormControl margin='normal' fullWidth>
        <FormLabel>{labels.week}</FormLabel>
        <DatePicker
          {...weekStartDayFieldProps}
          onChange={handleWeekStartDayChange}
          shouldDisableDate={isInvalidDietStartDay}
          inputVariant='outlined'
          format='d/M/yyyy'
        />
      </FormControl>
      <FormControl component='fieldset' margin='dense' fullWidth>
        <FormLabel component='legend'>{labels.dietSource}</FormLabel>
        <RadioGroup
          aria-label='source'
          name='dietSource'
          value={dietSource}
          onChange={(_, value) => handleDietSourceChange(value as DietSource)}
        >
          <FormControlLabel value={DietSource.Copy} control={<Radio />} label='Skopiuj istniejącą dietę' />
          <FormControlLabel value={DietSource.Generated} control={<Radio />} label='Wygeneruj nową dietę' />
        </RadioGroup>
      </FormControl>
      <Divider />
      {
        dietSource === DietSource.Copy
          ? (
            <CopyDietControls
              userId={values.userId}
              onDraftDietSelected={handleDietChange}
            />
          )
          : (
            <GenerateDietControls
              onDraftDietSelected={handleDietChange}
            />
          )
      }
      <Divider />
      {orderedMealPlan && <MealPlanPresenter orderedMealPlan={orderedMealPlan} />}
      {
        orderedMealPlan && (
          <Box display='flex' justifyContent='flex-end'>
            <Button type='submit' variant='contained' color='primary' disabled={isSubmitting}>
              Zapisz
            </Button>
          </Box>
        )
      }
      {
        status && (
          <Typography color='error'>
            {status}
          </Typography>
        )
      }
    </form>
  )
}
