import React, { FC, useMemo } from 'react'
import { Avatar, List, ListItem, ListItemAvatar, ListItemText, Typography, Fab } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import { useHistory } from 'react-router-dom'
import { differenceInMilliseconds } from 'date-fns'
import { ListItemLink, MainContent } from '~/components'
import { WeeklyUserDiet } from '~/models'
import { routes } from '../routes'
import { useDiets } from '../shared'
import './diets-list.css'

const sortDiets = (first: WeeklyUserDiet, second: WeeklyUserDiet): number => {
  const { user: firstUser, weekStartDay: firstWeekStartDay } = first
  const { user: secondUser, weekStartDay: secondWeekStartDay } = second

  if (firstUser.id === secondUser.id) {
    return differenceInMilliseconds(firstWeekStartDay, secondWeekStartDay)
  }

  if (firstUser.name === secondUser.name) {
    return firstUser.id > secondUser.id ? 1 : -1
  }

  return firstUser.name > secondUser.name ? 1 : -1
}

export const DietsList: FC = () => {
  const { diets: rawDiets, error } = useDiets()
  const history = useHistory()

  const handleAddItem = (): void => {
    history.push(routes.add())
  }

  const loading = rawDiets === undefined
  const diets = useMemo(
    () => (rawDiets || []).sort(sortDiets),
    [rawDiets]
  )

  return (
    <MainContent loading={loading} error={error}>
      <div className='diets-list'>
        <Typography className='diets-list__title' variant='h5'>
          Diety
        </Typography>
        <List>
          {
            diets.length === 0
              ? (
                <ListItem>
                  <ListItemText primary='Brak diet' />
                </ListItem>
              )
              : diets.map(({ id, user, weekStartDay }) => {
                return (
                  <ListItemLink
                    key={id}
                    button
                    to={routes.edit(id)}
                  >
                    <ListItemAvatar>
                      <Avatar alt={user.name} src={user.pictureUrl} />
                    </ListItemAvatar>
                    <ListItemText
                      primary={user.name}
                      secondary={weekStartDay.toLocaleDateString()}
                    />
                  </ListItemLink>
                )
              })
          }
        </List>
        <Fab className='diets-list__add' color='secondary' aria-label='add' onClick={handleAddItem}>
          <Add />
        </Fab>
      </div>
    </MainContent>
  )
}
