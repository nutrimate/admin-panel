import React, { FC } from 'react'
import { Redirect, RouteComponentProps } from 'react-router-dom'
import { Box, Divider, Button } from '@material-ui/core'
import { MainContent } from '~/components'
import { orderMealPlan } from '~/models'
import { MealPlanPresenter } from '../shared'
import { routes } from '../routes'
import { WeeklyDietHeader } from './WeeklyDietHeader'
import { EditMealPlanEntryDialog } from './EditMealPlanEntryDialog'
import { useEditDietController } from './useEditDietController'

type EditWeeklyDietProps = RouteComponentProps<{ id: string }>

export const EditWeeklyDiet: FC<EditWeeklyDietProps> = ({ match }) => {
  const { id } = match.params
  const {
    diet,
    editedMealPlanEntry,
    setMealPlanEntryPath,
    clearMealPlanEntryPath,
    handleMealPlanEntryChange,
    saveDiet
  } = useEditDietController(id)

  if (!diet) {
    return (
      <Redirect to={routes.home()} />
    )
  }

  const orderedMealPlan = orderMealPlan(diet.mealPlan)

  return (
    <MainContent loading={false} padded>
      <WeeklyDietHeader user={diet.user} weekStartDay={diet.weekStartDay} />
      <Divider />
      <MealPlanPresenter orderedMealPlan={orderedMealPlan} onOpenEditMealPlanEntryDialog={setMealPlanEntryPath} />
      <EditMealPlanEntryDialog
        entry={editedMealPlanEntry}
        onClose={clearMealPlanEntryPath}
        onMealPlanEntryChange={handleMealPlanEntryChange}
      />
      <Box display='flex' justifyContent='flex-end'>
        <Button onClick={saveDiet} variant='contained' color='primary'>
          Zapisz
        </Button>
      </Box>
    </MainContent>
  )
}
