import { useEffect, useMemo, useState } from 'react'
import { MealPlanEntry, WeeklyUserDiet } from '~/models'
import { useDiets } from '../shared'

type MealPlanEntryPath = { day: number; mealNumber: number }

export type EditDietController = {
  error: Error | undefined;
  diet: WeeklyUserDiet | undefined;
  editedMealPlanEntry: MealPlanEntry | undefined;
  setMealPlanEntryPath: (day: number, mealNumber: number) => void;
  clearMealPlanEntryPath: () => void;
  handleMealPlanEntryChange: (mealPlanEntry: MealPlanEntry) => void;
  saveDiet: () => Promise<void>;
}

export const useEditDietController = (id: string): EditDietController => {
  const { diets = [], error, update } = useDiets()
  const sourceDiet = useMemo(
    () => diets.find(d => d.id === id),
    [diets, id]
  )

  const [diet, setDiet] = useState(sourceDiet)
  const [editedMealPlanEntryPath, setEditedMealPlanEntryPath] = useState<MealPlanEntryPath | null>(null)
  useEffect(
    () => {
      setDiet(sourceDiet)
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [id]
  )

  const editedMealPlanEntry = diet && editedMealPlanEntryPath
    ? diet.mealPlan.find(m => m.day === editedMealPlanEntryPath.day && m.mealNumber === editedMealPlanEntryPath.mealNumber)
    : undefined

  const setMealPlanEntryPath = (day: number, mealNumber: number): void => setEditedMealPlanEntryPath({ day, mealNumber })
  const clearMealPlanEntryPath = (): void => setEditedMealPlanEntryPath(null)
  const handleMealPlanEntryChange = (newMealPlanEntry: MealPlanEntry): void => {
    setDiet(diet => diet && ({
      ...diet,
      mealPlan: diet.mealPlan.map(entry => {
        if (entry.day !== newMealPlanEntry.day || entry.mealNumber !== newMealPlanEntry.mealNumber) {
          return entry
        }

        return newMealPlanEntry
      })
    }))
  }
  const saveDiet = async (): Promise<void> => {
    if (!diet) {
      return
    }

    await update(diet)
  }

  return {
    error,
    diet,
    editedMealPlanEntry,
    setMealPlanEntryPath,
    clearMealPlanEntryPath,
    handleMealPlanEntryChange,
    saveDiet
  }
}
