import React, { FC } from 'react'
import { Avatar, Typography } from '@material-ui/core'
import { User } from '~/models'
import './weekly-diet-header.css'

type WeeklyDietHeaderProps = {
    user: User;
    weekStartDay: Date;
}

export const WeeklyDietHeader: FC<WeeklyDietHeaderProps> = ({ user, weekStartDay }) => {
  return (
    <div className='weekly-diet-header'>
      <Avatar
        alt={user.name}
        src={user.pictureUrl}
      />
      <Typography variant='h6'>
        {user.name}
      </Typography>
      <Typography variant='subtitle1' color='textSecondary'>
        {weekStartDay.toLocaleDateString()}
      </Typography>
    </div>
  )
}
