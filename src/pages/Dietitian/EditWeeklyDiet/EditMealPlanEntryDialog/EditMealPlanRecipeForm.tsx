import React, { FC } from 'react'
import { Button, List, ListItem, Typography, DialogActions } from '@material-ui/core'
import { FormikProps } from 'formik'
import { FormAutocomplete, FormTextField } from '~/components'
import { Ingredient, Recipe, getRecipeIngredientNutritionFacts, NutritionFacts, getRecipeNutritionFacts } from '~/models'

const renderNutritionFacts = (facts: NutritionFacts): string =>
  [
    `Kalorie: ${facts.calories.toFixed()} kcal`,
    `Białko: ${facts.proteins.toFixed(2)} g`,
    `Tłuszcze: ${facts.fats.toFixed(2)} g`,
    `Węglowodany: ${facts.carbohydrates.toFixed(2)} g`
  ].join(' / ')

export type MealPlanRecipeFormValues = {
  recipeId: string;
  ingredients: { ingredient: Ingredient; amount: number }[];
}

type EditMealPlanRecipeFormProps = FormikProps<MealPlanRecipeFormValues> & {
  recipes: Recipe[];
  onClose: () => void;
}

export const EditMealPlanRecipeForm: FC<EditMealPlanRecipeFormProps> = props => {
  const { handleSubmit, values, setValues, isSubmitting, onClose, recipes } = props

  const recipeNutritionFacts = getRecipeNutritionFacts({ ingredients: values.ingredients })

  const handleRecipeChange = (recipeId: null | string | string[]): void => {
    if (typeof recipeId !== 'string') {
      return
    }

    const recipe = recipes.find(r => r.id === recipeId)
    setValues({ recipeId, ingredients: recipe?.ingredients || [] })
  }

  return (
    <form onSubmit={handleSubmit}>
      <FormAutocomplete
        name='recipeId'
        label='Przepis'
        placeholder='Wybierz przepis...'
        options={recipes}
        getOptionLabel={(r: Recipe) => r.name}
        getOptionValue={(r: Recipe) => r.id}
        onChange={handleRecipeChange}
        helperText={renderNutritionFacts(recipeNutritionFacts)}
      />
      <Typography variant='subtitle1'>
        Dostosuj składniki
      </Typography>
      <List>
        {
          values.ingredients.map((recipeIngredient, index) => {
            const { ingredient } = recipeIngredient
            const ingredientNutritionFacts = getRecipeIngredientNutritionFacts(recipeIngredient)

            return (
              <ListItem key={`ingredients.${index}.amount`}>
                <FormTextField
                  type='number'
                  name={`ingredients.${index}.amount`}
                  label={`${ingredient.name} - Ilość (${ingredient.unit.name})`}
                  placeholder='Podaj ilosć składnika'
                  helperText={renderNutritionFacts(ingredientNutritionFacts)}
                  className='ingredients-list-item__amount'
                />
              </ListItem>
            )
          })
        }
      </List>
      <DialogActions>
        <Button color='primary' disabled={isSubmitting} onClick={onClose}>
          Anuluj
        </Button>
        <Button type='submit' color='primary' disabled={isSubmitting}>
          OK
        </Button>
      </DialogActions>
    </form>
  )
}
