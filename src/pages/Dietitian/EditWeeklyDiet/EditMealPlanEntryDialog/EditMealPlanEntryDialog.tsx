import React, { FC } from 'react'
import { Dialog, DialogTitle, DialogContent, DialogContentText } from '@material-ui/core'
import { Formik } from 'formik'
import { MealPlanEntry } from '~/models'
import { useRecipes } from '../../../shared'
import { MealPlanRecipeFormValues, EditMealPlanRecipeForm } from './EditMealPlanRecipeForm'

type EditMealPlanEntryDialogProps = {
  entry?: MealPlanEntry;
  onMealPlanEntryChange: (entry: MealPlanEntry) => void;
  onClose: () => void;
}

export const EditMealPlanEntryDialog: FC<EditMealPlanEntryDialogProps> = props => {
  const { entry, onClose, onMealPlanEntryChange } = props
  const initialValues: MealPlanRecipeFormValues = {
    recipeId: entry?.recipe.id || '',
    ingredients: entry?.recipe.ingredients || []
  }

  const { recipes = [], error } = useRecipes()

  const handleRecipeChange = (values: MealPlanRecipeFormValues): void => {
    if (!entry) {
      throw new Error('Can not set a recipe for meal plan entry when entry is not provided')
    }

    const recipe = recipes.find(r => r.id === values.recipeId)
    if (!recipe) {
      throw new Error(`Could not find recipe with id: ${values.recipeId}`)
    }

    const mealPlanEntry = {
      ...entry,
      recipe: {
        ...recipe,
        ingredients: values.ingredients
      }
    }
    onMealPlanEntryChange(mealPlanEntry)
    onClose()
  }

  return (
    <Dialog open={entry != null} maxWidth='md' fullWidth onClose={onClose}>
      <DialogTitle>
        Zmień przepis
      </DialogTitle>
      <DialogContent>
        {
          error
            ? (
              <DialogContentText>
                {error}
              </DialogContentText>
            )
            : (
              <Formik
                initialValues={initialValues as MealPlanRecipeFormValues}
                onSubmit={handleRecipeChange}
              >
                {
                  formik => (
                    <EditMealPlanRecipeForm {...formik} recipes={recipes} onClose={onClose} />
                  )
                }
              </Formik>
            )
        }
      </DialogContent>
    </Dialog>
  )
}
