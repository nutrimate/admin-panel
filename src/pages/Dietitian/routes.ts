export const routes = {
  home: () => '/diets',
  edit: (id: string) => `/diets/${id}`,
  add: () => '/diets/new'
}
