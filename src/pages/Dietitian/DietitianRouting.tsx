import React, { ReactElement } from 'react'
import { Route } from 'react-router-dom'
import { History } from 'history'
import { routes } from './routes'
import { DietsList } from './DietsList'
import { EditWeeklyDiet } from './EditWeeklyDiet'
import { AddDiet } from './AddDiet'

export type DietitianRoutingProps = {
  history: History;
}

export const DietitianRouting = ({ history }: DietitianRoutingProps): ReactElement[] => {
  return [
    <Route
      key='add-diet'
      path={routes.add()}
      component={() => (
        <AddDiet onDietCreated={() => history.push(routes.home())} />
      )}
    />,
    <Route
      key='edit-diet'
      path={routes.edit(':id')}
      component={EditWeeklyDiet}
    />,
    <Route
      key='diets'
      path={routes.home()}
      component={DietsList}
      exact
    />
  ]
}
