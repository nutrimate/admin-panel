import React, { FC } from 'react'
import {
  Avatar,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  ListSubheader
} from '@material-ui/core'
import { Edit as EditIcon } from '@material-ui/icons'
import { DailyMealPlan, getMealPlanTotalNutritions, getRecipeNutritionFacts } from '~/models'
import { NutritionSummary } from '../../../../shared'

type DailyMealPlanPresenterProps = {
  day: number;
  dailyMealPlan: DailyMealPlan;
  onOpenEditMealPlanEntryDialog?: (day: number, mealNumber: number) => void;
}

export const DailyMealPlanPresenter: FC<DailyMealPlanPresenterProps> = props => {
  const { day, dailyMealPlan, onOpenEditMealPlanEntryDialog } = props
  const orderedMealPlan = Object.keys(dailyMealPlan)
    .map(m => Number(m))
    .sort()
    .map(mealNumber => dailyMealPlan[mealNumber])

  const totals = getMealPlanTotalNutritions(orderedMealPlan)

  return (
    <List subheader={(
      <ListSubheader component='div'>
        <NutritionSummary {...totals} title={`Dzień ${day}`} />
      </ListSubheader>
    )}
    >
      {
        orderedMealPlan
          .map(mealPlanEntry => {
            const { meal, recipe, mealNumber } = mealPlanEntry
            const recipeNutrition = getRecipeNutritionFacts(recipe)

            return (
              <ListItem key={mealNumber}>
                <ListItemAvatar>
                  <Avatar>{mealNumber}</Avatar>
                </ListItemAvatar>
                <ListItemText primary={recipe.name} secondary={<NutritionSummary {...recipeNutrition} title={meal.name} />} />
                {
                  onOpenEditMealPlanEntryDialog && (
                    <ListItemSecondaryAction>
                      <IconButton edge='end' aria-label='delete' onClick={() => onOpenEditMealPlanEntryDialog(day, mealNumber)}>
                        <EditIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  )
                }
              </ListItem>
            )
          })
      }
    </List>
  )
}
