import React, { FC } from 'react'
import { Divider, ListSubheader } from '@material-ui/core'
import { DailyMealPlan, getMealPlanTotalNutritions, MealPlanEntry, NutritionFacts, OrderedMealPlan } from '~/models'
import { NutritionSummary } from '../../../shared'
import { DailyMealPlanPresenter } from './DailyMealPlanPresenter'

export type MealPlanPresenterProps = {
  orderedMealPlan: OrderedMealPlan;
  onOpenEditMealPlanEntryDialog?: (day: number, mealNumber: number) => void;
}

export const MealPlanPresenter: FC<MealPlanPresenterProps> = ({ orderedMealPlan, onOpenEditMealPlanEntryDialog }) => {
  const days = Object.keys(orderedMealPlan)
    .map(key => Number(key))
    .sort((a, b) => a - b)
  const numberOfDays = days.length

  const dailyMealPlans = days.map(day => ({ day, dailyMealPlan: orderedMealPlan[day] }))
  const allMealPlanEntries = dailyMealPlans
    .map(d => d.dailyMealPlan)
    .reduce(
      (allEntries: MealPlanEntry[], dailyMealPlan: DailyMealPlan) => {
        const entries = Object.values(dailyMealPlan)
        return allEntries.concat(entries)
      },
      []
    )

  const mealPlanTotals = getMealPlanTotalNutritions(allMealPlanEntries)
  const dailyTotals: NutritionFacts = {
    calories: mealPlanTotals.calories / numberOfDays,
    proteins: mealPlanTotals.proteins / numberOfDays,
    fats: mealPlanTotals.fats / numberOfDays,
    carbohydrates: mealPlanTotals.carbohydrates / numberOfDays
  }

  return (
    <div>
      {
        dailyMealPlans
          .map(({ day, dailyMealPlan }) => {
            return (
              <DailyMealPlanPresenter
                key={day}
                day={day}
                dailyMealPlan={dailyMealPlan}
                onOpenEditMealPlanEntryDialog={onOpenEditMealPlanEntryDialog}
              />
            )
          })
      }
      <Divider />
      <ListSubheader component='div'>
        <NutritionSummary {...dailyTotals} title='Dziennie' />
      </ListSubheader>
    </div>
  )
}
