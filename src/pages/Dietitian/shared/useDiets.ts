import useSWR from 'swr'
import { WeeklyDiet, WeeklyUserDiet } from '~/models'
import { useDietitianApiClient } from '~/infrastructure'
import { useUsers } from './useUsers'

export type DietsController = {
  diets?: WeeklyUserDiet[];
  error?: Error;
  create: (diet: WeeklyDiet) => Promise<void>;
  update: (diet: WeeklyDiet) => Promise<void>;
}

export const useDiets = (): DietsController => {
  const apiClient = useDietitianApiClient()
  const { users = [], error: usersError } = useUsers()
  const { data: diets, error: dietsError, mutate } = useSWR('diets', () => apiClient.getDiets())

  const create = async (diet: WeeklyDiet): Promise<void> => {
    await apiClient.createDiet(diet)
    const newData = (diets || []).concat(diet)
    await mutate(newData, true)
  }

  const update = async (diet: WeeklyDiet): Promise<void> => {
    await apiClient.updateDiet(diet)
    const newData = (diets || []).map(d => d.id === diet.id ? diet : d)
    await mutate(newData, true)
  }

  const userDiets = diets == null
    ? undefined
    : diets
      .map(d => {
        const user = users.find(u => u.id === d.userId)
        if (!user) {
          return null
        }

        const diet: WeeklyUserDiet = {
          ...d,
          user
        }
        return diet
      })
      .filter((d): d is WeeklyUserDiet => d != null)

  return {
    diets: userDiets,
    error: usersError || dietsError,
    create,
    update
  }
}
