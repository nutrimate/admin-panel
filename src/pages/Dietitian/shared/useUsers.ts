import useSWR from 'swr'
import { User } from '~/models'
import { useDietitianApiClient } from '~/infrastructure'

export type UsersController = {
  users?: User[];
  error?: Error;
}

export const useUsers = (): UsersController => {
  const apiClient = useDietitianApiClient()
  const { data: users, error } = useSWR('users', () => apiClient.getUsers())

  return {
    users,
    error
  }
}
