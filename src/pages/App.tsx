import { Redirect, Switch, useHistory } from 'react-router-dom'
import React, { FC } from 'react'
import { CircularProgress } from '@material-ui/core'
import { NoAccess } from '~/pages/NoAccess'
import { useNavigation } from './useNavigation'
import { CookbookRouting, routes as cookbookRoutes } from './Cookbook'
import { AppShell } from './AppShell'
import { DietitianRouting } from './Dietitian'
import { useRequiredPermissions } from './useRequiredPermissions'

export const App: FC = () => {
  const { sections, activeItem } = useNavigation()
  const history = useHistory()
  const hasRequiredPermissions = useRequiredPermissions()

  if (hasRequiredPermissions == null) {
    return (
      <AppShell title='' navigationSections={[]}>
        <CircularProgress />
      </AppShell>
    )
  }

  if (hasRequiredPermissions) {
    return (
      <AppShell title={activeItem?.name || ''} navigationSections={sections}>
        <Switch>
          {CookbookRouting()}
          {DietitianRouting({ history })}
          <Redirect to={cookbookRoutes.recipes.home()} />
        </Switch>
      </AppShell>
    )
  }

  return (
    <NoAccess />
  )
}
