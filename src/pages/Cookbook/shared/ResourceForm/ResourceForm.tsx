import React, { ComponentType, ReactElement } from 'react'
import { Formik, FormikProps } from 'formik'
import { Button, Typography } from '@material-ui/core'
import './resource-form.css'

export type ResourceFormProps<T> = {
  initialValues: Partial<T>;
  title: string;
  validationSchema: unknown;
  onSave: (value: T) => Promise<void>;
  content: ComponentType<FormikProps<T>>;
}

export function ResourceForm<T> ({ title, content, initialValues, validationSchema, onSave }: ResourceFormProps<T>): ReactElement {
  const Content = content
  return (
    <Formik
      initialValues={initialValues as T}
      validationSchema={validationSchema}
      onSubmit={async (values, { setSubmitting }) => {
        try {
          await onSave(values)
        } finally {
          setSubmitting(false)
        }
      }}
    >
      {
        formikBag => (
          <form className='resource-form' onSubmit={formikBag.handleSubmit}>
            <Typography className='resource-form__title' variant='h5'>
              {title}
            </Typography>
            <div className='resource-form__content'>
              <Content {...formikBag} />
              <div className='resource-form__buttons'>
                <Button type='submit' variant='contained' color='primary' disabled={formikBag.isSubmitting}>
                  Zapisz
                </Button>
              </div>
            </div>
          </form>
        )
      }
    </Formik>
  )
}
