import React, { ReactElement, ReactNode, useCallback } from 'react'
import { Switch, Route, RouteComponentProps, Redirect } from 'react-router-dom'
import { MainContent } from '../../../../components'

export type ResourceRouteProps<T extends { id: string } = { id: string }> = {
  items?: T[];
  error?: Error;
  routes: {
    home: () => string;
    new: () => string;
    edit: (id: string) => string;
  };
  renderHome: () => ReactNode;
  renderNew: () => ReactNode;
  renderEdit: (item: T) => ReactNode;
}

export function ResourceRoute<T extends { id: string }> (props: ResourceRouteProps<T>): ReactElement {
  const { error, items, routes, renderNew, renderEdit, renderHome } = props

  const renderEditRoute = useCallback(
    ({ match }: RouteComponentProps<{ id: string }>) => {
      if (!items) {
        return null
      }

      const { id } = match.params
      const item = items.find(i => i.id === id)
      if (!item) {
        return (
          <Redirect to={routes.home()} />
        )
      }

      return renderEdit(item)
    },
    [items, renderEdit, routes]
  )

  return (
    <MainContent error={error} loading={items === undefined}>
      <Switch>
        <Route
          path={routes.new()}
          render={renderNew}
        />
        <Route
          path={routes.edit(':id')}
          render={renderEditRoute}
        />
        <Route
          render={renderHome}
        />
      </Switch>
    </MainContent>
  )
}
