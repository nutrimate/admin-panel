import useSWR from 'swr'
import { useCookbookApiClient } from '../../../infrastructure'
import { Unit } from '../../../models'

export type UnitsController = {
  units?: Unit[];
  error?: Error;
  create: (unit: Unit) => Promise<void>;
  update: (unit: Unit) => Promise<void>;
}

export const useUnits = (): UnitsController => {
  const apiClient = useCookbookApiClient()
  const { data: units, error, mutate } = useSWR('units', () => apiClient.getAllUnits())
  const create = async (unit: Unit): Promise<void> => {
    await apiClient.createUnit(unit)
    const newUnits = (units || []).concat(unit)
    await mutate(newUnits, true)
  }

  const update = async (unit: Unit): Promise<void> => {
    await apiClient.updateUnit(unit)
    const newUnits = (units || []).map(u => u.id === unit.id ? unit : u)
    await mutate(newUnits, true)
  }

  return {
    units,
    error,
    create,
    update
  }
}
