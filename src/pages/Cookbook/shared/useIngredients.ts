import useSWR from 'swr'
import { useCookbookApiClient } from '../../../infrastructure'
import { Ingredient } from '../../../models'

export type IngredientsController = {
  ingredients?: Ingredient[];
  error?: Error;
  create: (ingredient: Ingredient) => Promise<void>;
  update: (ingredient: Ingredient) => Promise<void>;
}

export const useIngredients = (): IngredientsController => {
  const apiClient = useCookbookApiClient()
  const { data: ingredients, error, mutate } = useSWR('ingredients', () => apiClient.getAllIngredients())
  const create = async (ingredient: Ingredient): Promise<void> => {
    await apiClient.createIngredient(ingredient)
    const newIngredients = (ingredients || []).concat(ingredient)
    await mutate(newIngredients, true)
  }

  const update = async (ingredient: Ingredient): Promise<void> => {
    await apiClient.updateIngredient(ingredient)
    const newIngredients = (ingredients || []).map(i => i.id === ingredient.id ? ingredient : i)
    await mutate(newIngredients, true)
  }

  return {
    ingredients,
    error,
    create,
    update
  }
}
