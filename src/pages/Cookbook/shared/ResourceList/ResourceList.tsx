import React, { FC, ReactElement } from 'react'
import { Link } from 'react-router-dom'
import { Avatar, Fab, List, ListItem, ListItemAvatar, ListItemText, Typography } from '@material-ui/core'
import { Add } from '@material-ui/icons'
import './resource-list.css'

export type ListViewProps = {
  title: string;
  items: { key: string; title: string; subtitle?: ReactElement; url: string; imageUrl?: string }[];
  onAddItem: () => void;
}

export const ResourceList: FC<ListViewProps> = ({ items, title, onAddItem }) => {
  return (
    <div className='resource-list'>
      <Typography className='resource-list__title' variant='h5'>
        {title}
      </Typography>
      <List>
        {
          items
            .sort((firstItem, secondItem) => {
              const first = firstItem.title.toLowerCase()
              const second = secondItem.title.toLowerCase()

              if (first === second) {
                return 0
              }

              return first < second ? -1 : 1
            })
            .map(i => (
              <ListItem button key={i.key} component={Link} to={i.url}>
                {i.imageUrl && (
                  <ListItemAvatar>
                    <Avatar variant='square' alt={i.title} src={i.imageUrl}>
                      {i.title.substr(0, 1).toUpperCase()}
                    </Avatar>
                  </ListItemAvatar>
                )}
                <ListItemText primary={i.title} secondary={i.subtitle} />
              </ListItem>
            ))
        }
      </List>
      <Fab className='resource-list__add' color='secondary' aria-label='add' onClick={onAddItem}>
        <Add />
      </Fab>
    </div>
  )
}
