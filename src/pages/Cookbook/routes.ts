export const routes = {
  recipes: {
    home: () => '/recipes',
    new: () => '/recipes/new',
    edit: (id: string) => `/recipes/${id}`
  },
  ingredients: {
    home: () => '/ingredients',
    new: () => '/ingredients/new',
    edit: (id: string) => `/ingredients/${id}`
  },
  units: {
    home: () => '/units',
    new: () => '/units/new',
    edit: (id: string) => `/units/${id}`
  }
}
