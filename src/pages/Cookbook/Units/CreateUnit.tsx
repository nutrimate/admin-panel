import React, { FC } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { Unit } from '~/models'
import { EditUnitForm, FormUnit } from './EditUnitForm'

export type CreateUnitProps = {
  onCreate: (unit: Unit) => Promise<void>;
}

export const CreateUnit: FC<CreateUnitProps> = ({ onCreate }) => {
  const handleCreate = (unit: FormUnit): Promise<void> => onCreate({ ...unit, id: uuidv4() })

  return (
    <EditUnitForm
      title='Dodaj nową jednostkę'
      onSave={handleCreate}
    />
  )
}
