import React, { FC } from 'react'
import * as yup from 'yup'
import { Unit } from '~/models'
import { FormTextField } from '~/components'
import { ResourceForm } from '../shared'

export type FormUnit = Omit<Unit, 'id'>

const fieldLabels = {
  name: 'Nazwa',
  plurals: {
    0: 'Liczebnik - 1',
    1: 'Liczebnik - 2',
    2: 'Liczebnik - 5'
  }
}

const validationSchema = yup.object<FormUnit>().shape({
  name: yup.string().required().label(fieldLabels.name),
  plurals: yup.object().required().shape({
    0: yup.string().required().label(fieldLabels.plurals['0']),
    1: yup.string().required().label(fieldLabels.plurals['1']),
    2: yup.string().required().label(fieldLabels.plurals['2'])
  })
})

export type EditUnitFormProps = {
  unit?: FormUnit;
  title: string;
  onSave: (unit: FormUnit) => Promise<void>;
}

const FormContent: FC = () => (
  <>
    <FormTextField
      name='name'
      label={fieldLabels.name}
      placeholder='Wpisz nazwę'
      fullWidth
    />
    <FormTextField
      name='plurals.0'
      label={fieldLabels.plurals['0']}
      placeholder='Wpisz liczebnik dla 1 sztuki'
      helperText='np. 1 ząbek'
      fullWidth
    />
    <FormTextField
      name='plurals.1'
      label={fieldLabels.plurals['1']}
      placeholder='Wpisz liczebnik dla 2 sztuk'
      helperText='np. 2 ząbki'
      fullWidth
    />
    <FormTextField
      name='plurals.2'
      label={fieldLabels.plurals['2']}
      placeholder='Wpisz liczebnik dla 5 sztuk'
      helperText='np. 5 ząbków'
      fullWidth
    />
  </>
)

export const EditUnitForm: FC<EditUnitFormProps> = ({ title, unit, onSave }) => {
  const defaultUnit: FormUnit = { name: '', plurals: { 0: '', 1: '', 2: '' } }
  return (
    <ResourceForm
      title={title}
      initialValues={unit || defaultUnit}
      validationSchema={validationSchema}
      onSave={onSave}
      content={FormContent}
    />
  )
}
