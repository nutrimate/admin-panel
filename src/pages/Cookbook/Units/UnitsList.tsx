import React, { FC } from 'react'
import { useHistory } from 'react-router-dom'
import { Unit } from '~/models'
import { ResourceList } from '../shared'
import { routes } from '../routes'

export type UnitsListProps = {
  units: Unit[];
}

export const UnitsList: FC<UnitsListProps> = ({ units }) => {
  const history = useHistory()
  const items = units
    .map(u => ({ key: u.id, title: u.name, url: routes.units.edit(u.id) }))

  return (
    <ResourceList
      title='Jednostki'
      items={items}
      onAddItem={() => history.push(routes.units.new())}
    />
  )
}
