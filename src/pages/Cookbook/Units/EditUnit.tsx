import React, { FC } from 'react'
import { Unit } from '~/models'
import { EditUnitForm, FormUnit } from './EditUnitForm'

export type EditUnitProps = {
  unit: Unit;
  onEdit: (unit: Unit) => Promise<void>;
}

export const EditUnit: FC<EditUnitProps> = ({ unit, onEdit }) => {
  const handleEdit = (newUnit: FormUnit): Promise<void> => onEdit({ ...newUnit, id: unit.id })

  return (
    <EditUnitForm
      title='Zmień jednostkę'
      unit={unit}
      onSave={handleEdit}
    />
  )
}
