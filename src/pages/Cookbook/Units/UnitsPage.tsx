import React, { FC } from 'react'
import { useHistory } from 'react-router-dom'
import { Unit } from '~/models'
import { ResourceRoute, useUnits } from '../shared'
import { routes } from '../routes'
import { EditUnit } from './EditUnit'
import { UnitsList } from './UnitsList'
import { CreateUnit } from './CreateUnit'

export const UnitsPage: FC = () => {
  const history = useHistory()
  const { units, error, create, update } = useUnits()
  const navigateToUnits = (): void => history.push(routes.units.home())
  const handleCreate = (unit: Unit): Promise<void> => create(unit).then(navigateToUnits)
  const handleUpdate = (unit: Unit): Promise<void> => update(unit).then(navigateToUnits)

  return (
    <ResourceRoute
      items={units}
      error={error}
      routes={routes.units}
      renderHome={() => (<UnitsList units={units || []} />)}
      renderNew={() => (<CreateUnit onCreate={handleCreate} />)}
      renderEdit={(unit: Unit) => (<EditUnit onEdit={handleUpdate} unit={unit} />)}
    />
  )
}
