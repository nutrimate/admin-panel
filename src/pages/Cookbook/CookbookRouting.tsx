import React, { ReactElement } from 'react'
import { Route } from 'react-router-dom'
import { routes } from './routes'
import { RecipesPage } from './Recipes'
import { IngredientsPage } from './Ingredients'
import { UnitsPage } from './Units'

export const CookbookRouting = (): ReactElement[] => {
  const { recipes, ingredients, units } = routes

  return (
    [
      <Route
        key='recipes'
        path={recipes.home()}
        component={RecipesPage}
      />,
      <Route
        key='ingredients'
        path={ingredients.home()}
        component={IngredientsPage}
      />,
      <Route
        key='units'
        path={units.home()}
        component={UnitsPage}
      />
    ]
  )
}
