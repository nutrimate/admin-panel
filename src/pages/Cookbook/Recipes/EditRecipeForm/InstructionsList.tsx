import React, { FC } from 'react'
import { ArrayHelpers } from 'formik'
import { Button, IconButton, Typography } from '@material-ui/core'
import { Add, Delete } from '@material-ui/icons'
import { FormTextField } from '~/components'
import { fieldLabels } from './fieldLabels'
import './instructions-list.css'

export type InstructionsListProps = {
  arrayHelpers: ArrayHelpers;
  instructions: string[];
  error?: unknown;
}

export const InstructionsList: FC<InstructionsListProps> = (props) => {
  const { arrayHelpers, instructions, error } = props

  return (
    <div>
      <Typography variant='h6'>
        {fieldLabels.instructions.list}
      </Typography>
      {
        instructions.map((_, index) => (
          <div key={index} className='instructions-list-item'>
            <FormTextField
              name={`instructions.${index}`}
              label={`${fieldLabels.instructions.description} ${index + 1}`}
              placeholder='Opisz pojedynczy krok przepisu'
              className='instructions-list-item__description'
            />
            <IconButton
              aria-label='delete'
              onClick={() => arrayHelpers.remove(index)}
              className='instructions-list-item__delete'
            >
              <Delete fontSize='small' />
            </IconButton>
          </div>
        ))
      }
      <Button
        onClick={() => arrayHelpers.push('')}
        startIcon={<Add />}
      >
        Dodaj
      </Button>
      {
        error && typeof error === 'string' && (
          <Typography color='error'>
            {error}
          </Typography>
        )
      }
    </div>
  )
}
