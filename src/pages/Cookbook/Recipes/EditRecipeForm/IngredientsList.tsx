import React, { FC } from 'react'
import { ArrayHelpers } from 'formik'
import { IconButton, Typography, Button } from '@material-ui/core'
import { Add, Delete } from '@material-ui/icons'
import { Ingredient } from '~/models'
import { FormTextField, FormAutocomplete } from '~/components'
import { fieldLabels } from './fieldLabels'
import './ingredients-list.css'

export type IngredientsListProps = {
  arrayHelpers: ArrayHelpers;
  allIngredients: Ingredient[];
  recipeIngredients: { ingredientId: string; amount: number }[];
  error?: unknown;
}

export const IngredientsList: FC<IngredientsListProps> = (props) => {
  const { arrayHelpers, allIngredients, recipeIngredients, error } = props

  return (
    <div>
      <Typography variant='h6'>
        {fieldLabels.ingredients.list}
      </Typography>
      {
        recipeIngredients.map((ingredient, index) => (
          <div key={index} className='ingredients-list-item'>
            <FormAutocomplete
              name={`ingredients.${index}.ingredientId`}
              label={`${fieldLabels.ingredients.ingredient} ${index + 1}`}
              placeholder='Wybierz składnik'
              options={allIngredients}
              getOptionLabel={ingredient => ingredient.name}
              getOptionValue={ingredient => ingredient.id}
              className='ingredients-list-item__ingredient'
            />
            <FormTextField
              type='number'
              name={`ingredients.${index}.amount`}
              label={fieldLabels.ingredients.amount}
              placeholder='Podaj ilosć składnika'
              helperText={allIngredients.find(i => i.id === ingredient.ingredientId)?.unit.name}
              className='ingredients-list-item__amount'
            />
            <IconButton
              aria-label='delete'
              onClick={() => arrayHelpers.remove(index)}
              className='ingredients-list-item__delete'
            >
              <Delete fontSize='small' />
            </IconButton>
          </div>
        ))
      }
      <Button
        onClick={() => arrayHelpers.push('')}
        startIcon={<Add />}
      >
        Dodaj
      </Button>
      {
        error && typeof error === 'string' && (
          <Typography color='error'>
            {error}
          </Typography>
        )
      }
    </div>
  )
}
