export const fieldLabels = {
  name: 'Nazwa',
  imageUrl: 'Adres obrazka',
  timeToPrepare: 'Czas przygotowywania',
  mealTypes: 'Typy posiłków',
  ingredients: {
    list: 'Składniki',
    ingredient: 'Składnik',
    amount: 'Ilość'
  },
  instructions: {
    list: 'Instrukcje',
    description: 'Krok'
  },
  enabled: 'Aktywny'
}
