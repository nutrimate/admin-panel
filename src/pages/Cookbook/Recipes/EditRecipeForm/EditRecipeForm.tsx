import React, { FC, ReactNode } from 'react'
import * as yup from 'yup'
import { FieldArray, FormikProps } from 'formik'
import { Chip } from '@material-ui/core'
import { Ingredient, MealType, NormalizedRecipe, normalizeRecipe, Recipe } from '~/models'
import { FormSwitch, FormSelectField, FormTextField } from '~/components'
import { ResourceForm, useIngredients } from '../../shared'
import { useMealTypes } from '../../../shared'
import { IngredientsList } from './IngredientsList'
import { InstructionsList } from './InstructionsList'
import { fieldLabels } from './fieldLabels'

type FormRecipe = Omit<NormalizedRecipe, 'id'>

const ingredientSchema = yup.object().shape({
  ingredientId: yup.string().required().label(fieldLabels.ingredients.ingredient),
  amount: yup.number().required().moreThan(0).label(fieldLabels.ingredients.amount)
})

const instructionDescriptionSchema = yup.string().required().label(fieldLabels.instructions.description)

const validationSchema = yup.object<FormRecipe>().shape({
  name: yup.string().required().label(fieldLabels.name),
  imageUrl: yup.string().required().label(fieldLabels.imageUrl),
  timeToPrepare: yup.number().required().integer().moreThan(0).label(fieldLabels.timeToPrepare),
  mealTypes: yup.array().required().min(1).label(fieldLabels.mealTypes),
  ingredients: yup.array(ingredientSchema).required().min(1).label(fieldLabels.ingredients.list),
  instructions: yup.array(instructionDescriptionSchema).required().min(1).label(fieldLabels.instructions.list),
  enabled: yup.boolean().required().label(fieldLabels.enabled)
})

type FormContentProps = FormikProps<FormRecipe> & {
  mealTypes: MealType[];
  ingredients: Ingredient[];
  error?: Error;
}

const FormContent: FC<FormContentProps> = ({ values, errors, mealTypes, ingredients, error }) => {
  if (error) {
    return (
      <span>{error.message}</span>
    )
  }

  const renderSelectedMealType = (selected: unknown): ReactNode => {
    const selectedMealTypes = selected as (string[] | undefined)
    if (!selectedMealTypes || !selectedMealTypes.length) {
      return (
        <div style={{ opacity: 0.42 }}>
          Wybierz typ posiłku
        </div>
      )
    }

    return (
      <div>
        {selectedMealTypes
          .map(id => mealTypes.find(mt => mt.id === id) || { id, name: '' })
          .map(({ id, name }) => (
            <Chip key={id} label={name} />
          ))}
      </div>
    )
  }
  return (
    <>
      <FormTextField
        name='name'
        label={fieldLabels.name}
        placeholder='Wpisz nazwę'
      />
      <FormTextField
        name='imageUrl'
        label={fieldLabels.imageUrl}
        placeholder='Podaj adres obrazka'
      />
      <FormTextField
        name='timeToPrepare'
        type='number'
        label={fieldLabels.timeToPrepare}
        placeholder='Podaj czas przygotowywania'
      />
      <FormSelectField
        name='mealTypes'
        label={fieldLabels.mealTypes}
        options={mealTypes.map(mt => ({ key: mt.name, value: mt.id }))}
        selectProps={{ multiple: true, renderValue: renderSelectedMealType }}
      />
      <FieldArray
        name='instructions'
        render={arrayHelpers => (
          <InstructionsList
            arrayHelpers={arrayHelpers}
            instructions={values.instructions}
            error={errors.instructions}
          />
        )}
      />
      <FieldArray
        name='ingredients'
        render={arrayHelpers => (
          <IngredientsList
            arrayHelpers={arrayHelpers}
            recipeIngredients={values.ingredients}
            allIngredients={ingredients}
            error={errors.ingredients}
          />
        )}
      />
      <FormSwitch name='enabled' label={fieldLabels.enabled} />
    </>
  )
}

export type EditRecipeFormProps = {
  recipe?: Omit<Recipe, 'id'>;
  title: string;
  onSave: (recipe: Omit<Recipe, 'id'>) => Promise<void>;
}

export const EditRecipeForm: FC<EditRecipeFormProps> = ({ title, recipe, onSave }) => {
  const { mealTypes = [], error: mealTypesError } = useMealTypes()
  const { ingredients = [], error: ingredientsError } = useIngredients()
  const error = mealTypesError || ingredientsError

  const initialValues = recipe
    ? normalizeRecipe({ ...recipe, id: '' })
    : {
      name: '',
      imageUrl: '',
      timeToPrepare: 0,
      ingredients: [{ ingredientId: '', amount: 0 }],
      instructions: [''],
      mealTypes: [],
      enabled: true
    }

  const handleSave = (recipe: Omit<NormalizedRecipe, 'id'>): Promise<void> => {
    return onSave({
      ...recipe,
      ingredients: recipe.ingredients
        .map(i => {
          const ingredient = ingredients.find(ingredient => ingredient.id === i.ingredientId)
          if (!ingredient) {
            throw new Error('Invalid ingredient id ' + i.ingredientId)
          }
          return {
            ingredient,
            amount: i.amount
          }
        }),
      instructions: recipe.instructions
        .map((description, index) => ({
          description,
          step: index + 1
        })),
      mealTypes: recipe.mealTypes
        .map(id => {
          const mealType = mealTypes.find(t => t.id === id)
          if (!mealType) {
            throw new Error('Invalid meal type id ' + id)
          }

          return mealType
        })
    })
  }
  return (
    <ResourceForm
      title={title}
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSave={handleSave}
      content={props => (<FormContent {...props} mealTypes={mealTypes} ingredients={ingredients} error={error} />)}
    />
  )
}
