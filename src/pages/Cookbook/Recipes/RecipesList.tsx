import React, { FC } from 'react'
import { useHistory } from 'react-router-dom'
import { getRecipeNutritionFacts, Recipe } from '~/models'
import { ResourceList } from '../shared'
import { NutritionSummary } from '../../shared'
import { routes } from '../routes'

export type RecipesListProps = {
  recipes: Recipe[];
}

export const RecipesList: FC<RecipesListProps> = ({ recipes }) => {
  const history = useHistory()
  const items = recipes
    .map(r => {
      const nutritionFacts = getRecipeNutritionFacts(r)
      return {
        key: r.id,
        title: r.name,
        subtitle: (<NutritionSummary {...nutritionFacts} />),
        url: routes.recipes.edit(r.id),
        imageUrl: r.imageUrl
      }
    })

  return (
    <ResourceList
      title='Przepisy'
      items={items}
      onAddItem={() => history.push(routes.recipes.new())}
    />
  )
}
