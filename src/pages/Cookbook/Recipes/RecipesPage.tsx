
import React, { FC } from 'react'
import { useHistory } from 'react-router-dom'
import { Recipe } from '~/models'
import { useRecipes } from '../../shared'
import { ResourceRoute } from '../shared'
import { routes } from '../routes'
import { RecipesList } from './RecipesList'
import { CreateRecipe } from './CreateRecipe'
import { EditRecipe } from './EditRecipe'

export const RecipesPage: FC = () => {
  const history = useHistory()
  const { recipes, error, create, update } = useRecipes()
  const navigateToIngredients = (): void => history.push(routes.recipes.home())
  const handleCreate = (recipe: Recipe): Promise<void> => create(recipe).then(navigateToIngredients)
  const handleUpdate = (recipe: Recipe): Promise<void> => update(recipe).then(navigateToIngredients)

  return (
    <ResourceRoute
      items={recipes}
      error={error}
      routes={routes.recipes}
      renderHome={() => (<RecipesList recipes={recipes || []} />)}
      renderNew={() => (<CreateRecipe onCreate={handleCreate} />)}
      renderEdit={(recipe: Recipe) => (<EditRecipe recipe={recipe} onEdit={handleUpdate} />)}
    />
  )
}
