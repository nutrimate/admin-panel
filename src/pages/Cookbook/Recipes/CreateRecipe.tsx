import React, { FC } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { Recipe } from '~/models'
import { EditRecipeForm } from './EditRecipeForm'

export type CreateRecipeProps = {
  onCreate: (recipe: Recipe) => Promise<void>;
}

export const CreateRecipe: FC<CreateRecipeProps> = ({ onCreate }) => {
  const handleCreate = (recipe: Omit<Recipe, 'id'>): Promise<void> => onCreate({ ...recipe, id: uuidv4() })

  return (
    <EditRecipeForm
      title='Dodaj nowy przepis'
      onSave={handleCreate}
    />
  )
}
