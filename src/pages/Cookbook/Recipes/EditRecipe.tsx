import React, { FC } from 'react'
import { Recipe } from '~/models'
import { EditRecipeForm } from './EditRecipeForm'

export type EditRecipeProps = {
  recipe: Recipe;
  onEdit: (recipe: Recipe) => Promise<void>;
}

export const EditRecipe: FC<EditRecipeProps> = ({ recipe, onEdit }) => {
  const handleEdit = (newRecipe: Omit<Recipe, 'id'>): Promise<void> => onEdit({ ...newRecipe, id: recipe.id })
  const formRecipe = {
    ...recipe,
    ingredients: recipe.ingredients
  }
  return (
    <EditRecipeForm
      title='Zmień przepis'
      recipe={formRecipe}
      onSave={handleEdit}
    />
  )
}
