import React, { FC } from 'react'
import * as yup from 'yup'
import { Ingredient, Unit } from '~/models'
import { FormSelectField, FormTextField } from '~/components'
import { ResourceForm, useUnits } from '../shared'

export type FormIngredient = Omit<Ingredient, 'id' | 'unit'> & {
  unitId: string;
}

const fieldLabels = {
  name: 'Nazwa',
  unit: 'Jednostka',
  unitAmount: 'Ilość',
  calories: 'Kalorie',
  proteins: 'Białka',
  fats: 'Tłuszcze',
  carbohydrates: 'Węglowodany'
}

const validationSchema = yup.object<FormIngredient>().shape({
  name: yup.string().required().label(fieldLabels.name),
  unitId: yup.string().required().label(fieldLabels.unit),
  unitAmount: yup.number().required().label(fieldLabels.unitAmount).moreThan(0),
  calories: yup.number().required().label(fieldLabels.calories).min(0),
  proteins: yup.number().required().label(fieldLabels.proteins).min(0),
  fats: yup.number().required().label(fieldLabels.fats).min(0),
  carbohydrates: yup.number().required().label(fieldLabels.carbohydrates).min(0)
})

type FormContentProps = { units: Unit[]; error?: Error }

const FormContent: FC<FormContentProps> = ({ units, error }) => {
  if (error) {
    return (
      <span>{error.message}</span>
    )
  }

  return (
    <>
      <FormTextField
        name='name'
        label={fieldLabels.name}
        placeholder='Wpisz nazwę'
      />
      <FormSelectField
        name='unitId'
        label={fieldLabels.unit}
        options={[
          { key: 'Wybierz jednostkę...', value: '' },
          ...units.map(u => ({ key: u.name, value: u.id }))
        ]}
      />
      <FormTextField
        type='number'
        name='unitAmount'
        label={fieldLabels.unitAmount}
        placeholder='Podaj ilość jednostki'
      />
      <FormTextField
        type='number'
        name='calories'
        label={fieldLabels.calories}
        placeholder='Podaj ilość kalorii (kcal)'
      />
      <FormTextField
        type='number'
        name='proteins'
        label={fieldLabels.proteins}
        placeholder='Podaj ilość białka (g)'
      />
      <FormTextField
        type='number'
        name='fats'
        label={fieldLabels.fats}
        placeholder='Podaj ilość tłuszczu (g)'
      />
      <FormTextField
        type='number'
        name='carbohydrates'
        label={fieldLabels.carbohydrates}
        placeholder='Podaj ilość węglowodanów (g)'
      />
    </>
  )
}

export type EditIngredientFormProps = {
  ingredient?: Ingredient;
  title: string;
  onSave: (unit: Omit<Ingredient, 'id'>) => Promise<void>;
}

export const EditIngredientForm: FC<EditIngredientFormProps> = ({ title, ingredient, onSave }) => {
  const { units = [], error } = useUnits()

  const handleSave = (formIngredient: FormIngredient): Promise<void> => {
    const unit = units.find(u => u.id === formIngredient.unitId)
    if (!unit) {
      throw new Error('Tried to save form without choosing valid unit')
    }
    const ingredient = {
      ...formIngredient,
      unit
    }

    return onSave(ingredient)
  }

  const initialValues = ingredient ? { ...ingredient, unitId: ingredient.unit.id } : { unitId: '' }

  return (
    <ResourceForm
      title={title}
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSave={handleSave}
      content={() => (<FormContent units={units} error={error} />)}
    />
  )
}
