import React, { FC } from 'react'
import { v4 as uuidv4 } from 'uuid'
import { Ingredient } from '~/models'
import { EditIngredientForm } from './EditIngredientForm'

export type CreateIngredientProps = {
  onCreate: (ingredient: Ingredient) => Promise<void>;
}

export const CreateIngredient: FC<CreateIngredientProps> = ({ onCreate }) => {
  const handleCreate = (ingredient: Omit<Ingredient, 'id'>): Promise<void> => onCreate({ ...ingredient, id: uuidv4() })

  return (
    <EditIngredientForm
      title='Dodaj nowy składnik'
      onSave={handleCreate}
    />
  )
}
