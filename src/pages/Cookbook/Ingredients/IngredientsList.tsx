import React, { FC } from 'react'
import { useHistory } from 'react-router-dom'
import { Ingredient } from '~/models'
import { ResourceList } from '../shared'
import { routes } from '../routes'

export type IngredientsListProps = {
  ingredients: Ingredient[];
}

export const IngredientsList: FC<IngredientsListProps> = ({ ingredients }) => {
  const history = useHistory()
  const items = ingredients
    .map(i => ({ key: i.id, title: i.name, url: routes.ingredients.edit(i.id) }))
  return (
    <ResourceList
      title='Składniki'
      items={items}
      onAddItem={() => history.push(routes.ingredients.new())}
    />
  )
}
