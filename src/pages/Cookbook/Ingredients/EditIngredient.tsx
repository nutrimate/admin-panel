import React, { FC } from 'react'
import { Ingredient } from '~/models'
import { EditIngredientForm } from './EditIngredientForm'

export type EditIngredientProps = {
  ingredient: Ingredient;
  onEdit: (ingredient: Ingredient) => Promise<void>;
}

export const EditIngredient: FC<EditIngredientProps> = ({ ingredient, onEdit }) => {
  const handleEdit = (updated: Omit<Ingredient, 'id'>): Promise<void> => onEdit({ ...updated, id: ingredient.id })

  return (
    <EditIngredientForm
      title='Zmień składnik'
      ingredient={ingredient}
      onSave={handleEdit}
    />
  )
}
