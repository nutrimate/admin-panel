import React, { FC } from 'react'
import { useHistory } from 'react-router-dom'
import { Ingredient } from '~/models'
import { useIngredients, ResourceRoute } from '../shared'
import { routes } from '../routes'
import { EditIngredient } from './EditIngredient'
import { IngredientsList } from './IngredientsList'
import { CreateIngredient } from './CreateIngredient'

export const IngredientsPage: FC = () => {
  const history = useHistory()
  const { ingredients, error, create, update } = useIngredients()
  const navigateToIngredients = (): void => history.push(routes.ingredients.home())
  const handleCreate = (ingredient: Ingredient): Promise<void> => create(ingredient).then(navigateToIngredients)
  const handleUpdate = (ingredient: Ingredient): Promise<void> => update(ingredient).then(navigateToIngredients)

  return (
    <ResourceRoute
      items={ingredients}
      error={error}
      routes={routes.ingredients}
      renderHome={() => (<IngredientsList ingredients={ingredients || []} />)}
      renderNew={() => (<CreateIngredient onCreate={handleCreate} />)}
      renderEdit={(ingredient: Ingredient) => (<EditIngredient onEdit={handleUpdate} ingredient={ingredient} />)}
    />
  )
}
