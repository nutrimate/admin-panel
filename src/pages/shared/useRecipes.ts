import useSWR from 'swr'
import { useCookbookApiClient } from '../../infrastructure'
import { normalizeRecipe, Recipe } from '../../models'

export type RecipesController = {
  recipes?: Recipe[];
  error?: Error;
  create: (recipe: Recipe) => Promise<void>;
  update: (recipe: Recipe) => Promise<void>;
}

export const useRecipes = (): RecipesController => {
  const apiClient = useCookbookApiClient()
  const { data: recipes, error, mutate } = useSWR('recipes', () => apiClient.getAllRecipes())

  const create = async (recipe: Recipe): Promise<void> => {
    await apiClient.createRecipe(normalizeRecipe(recipe))
    const newRecipes = (recipes || []).concat(recipe)
    await mutate(newRecipes, true)
  }

  const update = async (recipe: Recipe): Promise<void> => {
    await apiClient.updateRecipe(normalizeRecipe(recipe))
    const newRecipes = (recipes || []).map(i => i.id === recipe.id ? recipe : i)
    await mutate(newRecipes, true)
  }

  return {
    recipes,
    error,
    create,
    update
  }
}
