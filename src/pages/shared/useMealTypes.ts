import useSWR from 'swr'
import { useCookbookApiClient } from '~/infrastructure'
import { MealType } from '~/models'

type MealTypesController = {
  mealTypes?: MealType[];
  error?: Error;
}

export const useMealTypes = (): MealTypesController => {
  const apiClient = useCookbookApiClient()
  const { data: mealTypes = [], error } = useSWR('meal-types', () => apiClient.getMealTypes())

  return {
    mealTypes,
    error
  }
}
