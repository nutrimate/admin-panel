import React, { FC } from 'react'
import './nutrition-summary.css'

export type NutritionSummaryProps = {
  title?: string;
  calories: number;
  proteins: number;
  fats: number;
  carbohydrates: number;
}

export const NutritionSummary: FC<NutritionSummaryProps> = props => {
  const { title, calories, proteins, fats, carbohydrates } = props

  return (
    <span className='nutrition-summary'>
      {title && (<span className='nutrition-summary__item'>{title}</span>)}
      <span className='nutrition-summary__item'>{Math.round(calories)} kcal</span>
      <span className='nutrition-summary__item'>B: {Math.round(proteins)} g</span>
      <span className='nutrition-summary__item'>T: {Math.round(fats)} g</span>
      <span className='nutrition-summary__item'>W: {Math.round(carbohydrates)} g</span>
    </span>
  )
}
