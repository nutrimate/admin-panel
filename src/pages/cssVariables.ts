import { makeStyles } from '@material-ui/core'

export const useThemeCssVariables = makeStyles(theme => ({
  root: {
    '--palette-error-main': theme.palette.error.main
  }
}))
