import { useEffect, useState } from 'react'
import jwtDecode from 'jwt-decode'
import { useAuth0 } from '@auth0/auth0-react'

export const requiredPermissions = [
  'write:cookbook',
  'admin:diets',
  'read:users'
]

export const useRequiredPermissions = (): boolean | undefined => {
  const { getAccessTokenSilently } = useAuth0()
  const [
    hasRequiredPermissions,
    setHasRequiredPermissions
  ] = useState<boolean | undefined>(undefined)

  useEffect(
    () => {
      const checkPermissions = async (): Promise<void> => {
        const token = await getAccessTokenSilently()

        const { permissions = [] } = jwtDecode<{ permissions?: string[] } | undefined>(token) || {}
        const hasRequiredPermissions = requiredPermissions.every(p => permissions.includes(p))
        setHasRequiredPermissions(hasRequiredPermissions)
      }

      checkPermissions()
    },
    [getAccessTokenSilently, setHasRequiredPermissions]
  )

  return hasRequiredPermissions
}
