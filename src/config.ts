export const config = {
  auth: {
    clientId: process.env.AUTH_CLIENT_ID,
    domain: process.env.AUTH_DOMAIN,
    audience: process.env.AUTH_AUDIENCE
  },
  sentry: {
    dsn: process.env.SENTRY_DSN,
    env: process.env.SENTRY_ENV
  },
  cookbookApiUrl: process.env.COOKBOOK_API_URL,
  dietitianApiUrl: process.env.DIETITIAN_API_URL,
  mealPlannerApiUrl: process.env.MEAL_PLANNER_API_URL
}
